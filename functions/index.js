const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

exports.sendDrainNotification = functions.database.ref('/events/{eventId}').onWrite(event => {
  console.info(event.data.val());
  if (!event.data.val()) {
    console.info('Empty event - leaving');
  }

  admin.messaging().sendToTopic('all', {
    data: event.data.val()
  });
});