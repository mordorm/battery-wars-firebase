# Firebase Stuff

This is all the firebase function stuff. Its main purpose is to allow clients
to post events to the database and then have the server translate that to a
firebase cloud message (so we can receive it if the app is closed).

To setup, run:

```bash
npm install -g firebase-tools
```

And then to deploy your changes, run:

```
firebase deploy
```
